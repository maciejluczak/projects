package hello;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Arrays;
import java.util.List;

import twitter4j.*;
import twitter4j.json.DataObjectFactory;
/**
 * Servlet implementation class ContentServlet
 */
@WebServlet("/ContentServlet")
public class ContentServlet extends HttpServlet {
	HelloWordTwitter helloTwitter;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        PrintWriter out = response.getWriter();
        try {       	
        	helloTwitter = new HelloWordTwitter();
        	String req = "";
        	if(request.getParameter("filtr")!="") req = "\""+request.getParameter("filtr")+"\" ";
        	if(request.getParameter("kanal")!="") req = req + "from:"+request.getParameter("kanal");
        	
    		Cookie cookie = new Cookie("filtersToTwetter", request.getParameter("filtr"));
    		cookie.setMaxAge(60*60*24);
    		response.addCookie(cookie);
    		cookie = new Cookie("chanelsToTwetter", request.getParameter("kanal"));
    		cookie.setMaxAge(60*60*24);
    		response.addCookie(cookie);
        	
        	List<Status> stat  = helloTwitter.getTwitts(req);
        	String json = "[";
        	for (Status status : stat) {
        		if(json=="[") json += DataObjectFactory.getRawJSON(status);
        		else json += "," + DataObjectFactory.getRawJSON(status);
			}
        	json += "]";
        	out.println(json);    	
        } catch (TwitterException e) {
			e.printStackTrace();
		}finally {
            out.close();
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
