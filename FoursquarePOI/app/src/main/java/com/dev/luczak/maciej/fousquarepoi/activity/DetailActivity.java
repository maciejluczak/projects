package com.dev.luczak.maciej.fousquarepoi.activity;

import android.content.Intent;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.luczak.maciej.fousquarepoi.R;

public class DetailActivity extends AppCompatActivity {

    private String mDetails;
    final static String VENUES_SHARE_HASHTAG = "#Venues";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detail);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }catch (Exception e){
            e.printStackTrace();
        }

        TextView mVenuesDisplay = (TextView) findViewById(R.id.display_venues_detail);
        Intent intentThatStartedThisActivity = getIntent();
        if (intentThatStartedThisActivity != null) {
            if (intentThatStartedThisActivity.hasExtra(Intent.EXTRA_TEXT)) {
                mDetails = intentThatStartedThisActivity.getStringExtra(Intent.EXTRA_TEXT);
                mVenuesDisplay.setText(mDetails);
            }
        }

    }

    private Intent createShareVenuesIntent() {
        return ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setText(mDetails +VENUES_SHARE_HASHTAG)
                .getIntent();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            Toast.makeText(this,"Share",Toast.LENGTH_SHORT).show();
            this.startActivity(Intent.createChooser(createShareVenuesIntent(),"Share..."));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
