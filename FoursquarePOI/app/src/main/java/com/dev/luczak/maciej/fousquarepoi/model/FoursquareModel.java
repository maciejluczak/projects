package com.dev.luczak.maciej.fousquarepoi.model;

import java.io.Serializable;

public class FoursquareModel implements Serializable {
    private String name;
    private int distance;
    private String address;
    private String crossStreet;
    private String categories;
    private String categoriesId;
    private int checkinsCount;
    private double latitude;
    private double longitude;

    public FoursquareModel(){
        this.distance = 0;
        this.address = "";
        this.crossStreet = "";
        this.categories = "";
        this.checkinsCount = 0;
        this.latitude = 0.0f;
        this.longitude = 0.0f;
        this.categoriesId ="";
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCrossStreet() {
        return crossStreet;
    }

    public void setCrossStreet(String crossStreet) {
        this.crossStreet = crossStreet;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public int getCheckinsCount() {
        return checkinsCount;
    }

    public void setCheckinsCount(int checkinsCount) {
        this.checkinsCount = checkinsCount;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCategoriesId() {
        return categoriesId;
    }

    public void setCategoriesId(String categoriesId) {
        this.categoriesId = categoriesId;
    }
}
