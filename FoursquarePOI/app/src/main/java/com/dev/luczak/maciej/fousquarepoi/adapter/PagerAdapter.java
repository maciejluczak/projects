package com.dev.luczak.maciej.fousquarepoi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class PagerAdapter extends FragmentPagerAdapter {

    private List<String> tabsTitlesList =new ArrayList<>();
    private List<Fragment> tabsList = new ArrayList<>();

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addTab(Fragment tabFrgment, String tabTitle){
        tabsList.add(tabFrgment);
        tabsTitlesList.add(tabTitle);
    }

    @Override
    public Fragment getItem(int position) {
        return tabsList.get(position);
    }

    @Override
    public int getCount() {
        return tabsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //return super.getPageTitle(position);
        return tabsTitlesList.get(position);
    }
}
