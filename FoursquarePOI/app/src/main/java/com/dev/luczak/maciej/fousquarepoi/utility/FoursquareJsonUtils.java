package com.dev.luczak.maciej.fousquarepoi.utility;

import com.dev.luczak.maciej.fousquarepoi.model.FoursquareModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class FoursquareJsonUtils {

    private final static String OBJ_RESPONSE = "response";
    private final static String OBJ_RESPONSE_VENUES = "venues";
    private final static String OBJ_RESPONSE_VENUES_NAME = "name";
    private final static String OBJ_RESPONSE_VENUES_LOCATION = "location";
    private final static String OBJ_RESPONSE_VENUES_LOCATION_DISTANCE = "distance";
    private final static String OBJ_RESPONSE_VENUES_LOCATION_ADDRESS = "address";
    private final static String OBJ_RESPONSE_VENUES_LOCATION_CROSS = "crossStreet";
    private final static String OBJ_RESPONSE_VENUES_LOCATION_LATITUDE = "lat";
    private final static String OBJ_RESPONSE_VENUES_LOCATION_LONGITIDE = "lng";
    private final static String OBJ_RESPONSE_VENUES_CATEGORIES = "categories";
    private final static String OBJ_RESPONSE_VENUES_CATEGORIES_NAME = "name";
    private final static String OBJ_RESPONSE_VENUES_CATEGORIES_ID = "id";
    private final static String OBJ_RESPONSE_VENUES_STATS = "stats";
    private final static String OBJ_RESPONSE_VENUES_STATS_CHECKINSCOUNT = "checkinsCount";
    private ArrayList<FoursquareModel> parsedPoiData;

    public FoursquareJsonUtils() {
        parsedPoiData = new ArrayList<>();
    }

    public ArrayList<FoursquareModel> getSimpleDataFrmJson(String poiJsonStr)
            throws JSONException {

        JSONObject poiJson = new JSONObject(poiJsonStr);
        if (poiJson.has(OBJ_RESPONSE)) {
            poiJson = poiJson.getJSONObject(OBJ_RESPONSE);
            if (poiJson.has(OBJ_RESPONSE_VENUES)) {
                JSONArray poiArray = poiJson.getJSONArray(OBJ_RESPONSE_VENUES);
                for (int i = 0; i < poiArray.length(); i++) {
                    FoursquareModel foursquareModel = new FoursquareModel();

                    JSONObject poiLocal = poiArray.getJSONObject(i);
                    if (poiLocal.has(OBJ_RESPONSE_VENUES_NAME)) {
                        poiLocal.getString(OBJ_RESPONSE_VENUES_NAME);
                        foursquareModel.setName(poiLocal.getString(OBJ_RESPONSE_VENUES_NAME));
                    }
                    if (poiLocal.has(OBJ_RESPONSE_VENUES_LOCATION)) {
                        JSONObject locationObject = poiLocal.getJSONObject(OBJ_RESPONSE_VENUES_LOCATION);
                        if (locationObject.has(OBJ_RESPONSE_VENUES_LOCATION_DISTANCE)) {
                            foursquareModel.setDistance(locationObject.getInt(OBJ_RESPONSE_VENUES_LOCATION_DISTANCE));
                        }
                        if (locationObject.has(OBJ_RESPONSE_VENUES_LOCATION_ADDRESS)) {
                            foursquareModel.setAddress(locationObject.getString(OBJ_RESPONSE_VENUES_LOCATION_ADDRESS));
                        }
                        if (locationObject.has(OBJ_RESPONSE_VENUES_LOCATION_CROSS)) {
                            foursquareModel.setCrossStreet(locationObject.getString(OBJ_RESPONSE_VENUES_LOCATION_CROSS));
                        }
                        if (locationObject.has(OBJ_RESPONSE_VENUES_LOCATION_LATITUDE)) {
                            foursquareModel.setLatitude(locationObject.getDouble(OBJ_RESPONSE_VENUES_LOCATION_LATITUDE));
                        }
                        if (locationObject.has(OBJ_RESPONSE_VENUES_LOCATION_LONGITIDE)) {
                            foursquareModel.setLongitude(locationObject.getDouble(OBJ_RESPONSE_VENUES_LOCATION_LONGITIDE));
                        }
                    }
                    if (poiLocal.has(OBJ_RESPONSE_VENUES_CATEGORIES)) {
                        JSONArray categoriesArray = poiLocal.getJSONArray(OBJ_RESPONSE_VENUES_CATEGORIES);
                        if(categoriesArray.length()>0){
                            JSONObject categoriesObject = categoriesArray.getJSONObject(0);
                            if (categoriesObject.has(OBJ_RESPONSE_VENUES_CATEGORIES_NAME)) {
                                foursquareModel.setCategories(categoriesObject.getString(OBJ_RESPONSE_VENUES_CATEGORIES_NAME));
                            }
                            if (categoriesObject.has(OBJ_RESPONSE_VENUES_CATEGORIES_ID)) {
                                foursquareModel.setCategoriesId(categoriesObject.getString(OBJ_RESPONSE_VENUES_CATEGORIES_ID));
                            }
                        }
                    }
                    if (poiLocal.has(OBJ_RESPONSE_VENUES_STATS)) {
                        JSONObject rateObject = poiLocal.getJSONObject(OBJ_RESPONSE_VENUES_STATS);
                        if (rateObject.has(OBJ_RESPONSE_VENUES_STATS_CHECKINSCOUNT)) {
                            foursquareModel.setCheckinsCount(rateObject.getInt(OBJ_RESPONSE_VENUES_STATS_CHECKINSCOUNT));
                        }
                    }

                    parsedPoiData.add(foursquareModel);
                }
            }
        }
        return parsedPoiData;
    }
}
