package com.dev.luczak.maciej.fousquarepoi.utility;

import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class NetworkUtils {

    private final static String TAG = "NetworkUtils";

    private final static String BASE_URL =
            "https://api.foursquare.com/v2/venues/search";

    private final static String PARAM_VERSION = "v";
    private final static String PARAM_LL = "ll";
    private final static String PARAM_QUERY = "query";
    private final static String PARAM_INTENT = "intent";
    private final static String PARAM_RADIUS = "radius";
    private final static String PARAM_CLIIENT_ID = "client_id";
    private final static String PARAM_CLIIENT_SECRET = "client_secret";

    private final static String PARAM_VERSION_VAL = "20170222";
    private final static String PARAM_QUERY_VAL = "coffee";
    private final static String PARAM_INTENT_VAL = "browse";
    private final static String PARAM_RADIUS_VAL = "20000";
    private final static String PARAM_CLIIENT_ID_VAL = "IDGCJZLKKQDD4F2SJTJY25PAISQKC1Q2JLFLKTHTH2BNZHSM";
    private final static String PARAM_CLIIENT_SECRET_VAL = "LG2FE0LIBOVWMDS0B5JMZOSARM3QQ3GICG4WWXOHTBJXID5O";

    public static URL buildUrl(double latitude, double longitude) {
        String ll_val = String.valueOf(latitude) + ", "+ String.valueOf(longitude);
        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendQueryParameter(PARAM_VERSION, PARAM_VERSION_VAL)
                .appendQueryParameter(PARAM_LL, ll_val)
                .appendQueryParameter(PARAM_QUERY, PARAM_QUERY_VAL)
                .appendQueryParameter(PARAM_INTENT, PARAM_INTENT_VAL)
                .appendQueryParameter(PARAM_RADIUS, PARAM_RADIUS_VAL)
                .appendQueryParameter(PARAM_CLIIENT_ID, PARAM_CLIIENT_ID_VAL)
                .appendQueryParameter(PARAM_CLIIENT_SECRET, PARAM_CLIIENT_SECRET_VAL)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Log.v(TAG, "Built URI " + url);
        return url;
    }

    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}