package com.dev.luczak.maciej.fousquarepoi.adapter;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.luczak.maciej.fousquarepoi.model.FoursquareModel;
import com.dev.luczak.maciej.fousquarepoi.R;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.TextItemViewHolder>  {

    private ArrayList<FoursquareModel> items;
    private final VenuesAdapterOnClickHandler mClickHandler;

    public RecyclerViewAdapter(VenuesAdapterOnClickHandler clickHandler) {
        mClickHandler = clickHandler;
    }

    public interface VenuesAdapterOnClickHandler {
        void onClick(String venuesDetail);
    }

    class TextItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final ImageView categories_icon;
        final TextView venues_name;
        final TextView distance;
        final TextView rank;
        final TextView categories;



        TextItemViewHolder(View itemView) {
            super(itemView);
            venues_name = (TextView) itemView.findViewById(R.id.venues_name);
            distance = (TextView) itemView.findViewById(R.id.distance);
            rank = (TextView) itemView.findViewById(R.id.rank);
            categories = (TextView) itemView.findViewById(R.id.categories);
            categories_icon = (ImageView) itemView.findViewById(R.id.categories_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            String venuesDetails = items.get(adapterPosition).getName() +"\n"+
                    items.get(adapterPosition).getAddress()+"\n"+
                    items.get(adapterPosition).getCrossStreet()+"\n"+
                    items.get(adapterPosition).getCategories();
            mClickHandler.onClick(venuesDetails);
        }
    }


    @Override
    public TextItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycer_view_list_item, parent, false);
        return new TextItemViewHolder(view);
    }


    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(TextItemViewHolder holder, int position) {

        if(items.get(position).getCategoriesId().equals("4bf58dd8d48988d16d941735")){
            holder.categories_icon.setImageResource(R.drawable.ic_coffee);
        }else if(items.get(position).getCategoriesId().equals("4bf58dd8d48988d1e0931735")){
            holder.categories_icon.setImageResource(R.drawable.ic_coffeeshop);
        }else {
            holder.categories_icon.setImageResource(R.drawable.ic_food);
        }
        holder.categories.setText(items.get(position).getCategories());
        holder.rank.setText(String.valueOf(items.get(position).getCheckinsCount()));
        String kmStr;
        if(items.get(position).getDistance()>1000) {
            Double km = (double) items.get(position).getDistance() / 1000;
            kmStr = String.format("%.2f", km) +" km";
        }else {
            kmStr = String.valueOf(items.get(position).getDistance())+" m";
        }
        holder.distance.setText(kmStr);
        holder.venues_name.setText(items.get(position).getName());

    }

    @Override
    public int getItemCount() {
        if(items != null) {
            return items.size();
        }else {
            return 0;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(ArrayList<FoursquareModel> items) {
        this.items = items;
    }
}
