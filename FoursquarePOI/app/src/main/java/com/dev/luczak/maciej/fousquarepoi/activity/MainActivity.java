package com.dev.luczak.maciej.fousquarepoi.activity;

import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.dev.luczak.maciej.fousquarepoi.utility.FoursquareJsonUtils;
import com.dev.luczak.maciej.fousquarepoi.model.FoursquareModel;
import com.dev.luczak.maciej.fousquarepoi.utility.NetworkUtils;
import com.dev.luczak.maciej.fousquarepoi.adapter.PagerAdapter;
import com.dev.luczak.maciej.fousquarepoi.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener, LoaderManager.LoaderCallbacks<ArrayList<FoursquareModel>> {

    TabLayout tabs;
    ViewPager pages;
    PagerAdapter pagerAdapter;
    private Location position;
    private GoogleApiClient mGoogleApiClient;
    private static final int VENUES_LOADER_ID = 0;
    public ArrayList<FoursquareModel> nearestList;
    public ArrayList<FoursquareModel> bestList;
    private static final String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }catch (Exception e){
            e.printStackTrace();
        }
        tabs = (TabLayout) findViewById(R.id.tab_layout);
        pages = (ViewPager) findViewById(R.id.view_pager);
        createPagerFragments();
        pages.setAdapter(pagerAdapter);
        tabs.setupWithViewPager(pages);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nearestList!=null) {
                    Class destinationClass = MapsActivity.class;
                    Intent intentToStartDetailActivity = new Intent(getApplicationContext(), destinationClass);
                    intentToStartDetailActivity.putExtra(getString(R.string.model_extra), nearestList);
                    intentToStartDetailActivity.putExtra(getString(R.string.latitude_extra), position.getLatitude());
                    intentToStartDetailActivity.putExtra(getString(R.string.longitude_extra), position.getLongitude());
                    startActivity(intentToStartDetailActivity);
                }
            }
        });
    }

    private void createPagerFragments() {
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addTab(new NearestPOI(), "Nearest");
        pagerAdapter.addTab(new BestPOI(), "Best");
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onResume() {
        mGoogleApiClient.connect();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mGoogleApiClient.disconnect();
        super.onPause();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest mLocationRequest;
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setSmallestDisplacement(1000);
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        position = location;

        int loaderId = VENUES_LOADER_ID;
        LoaderManager.LoaderCallbacks<ArrayList<FoursquareModel>> callback = MainActivity.this;
        getSupportLoaderManager().initLoader(loaderId, null, callback);
    }

    @Override
    public android.support.v4.content.Loader<ArrayList<FoursquareModel>> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<ArrayList<FoursquareModel>>(this) {

            ArrayList<FoursquareModel> mVenuesData = null;

            /**
             * Subclasses of AsyncTaskLoader must implement this to take care of loading their data.
             */
            @Override
            protected void onStartLoading() {
                if (mVenuesData != null) {
                    deliverResult(mVenuesData);
                } else {
                    forceLoad();
                }
            }

            /**
             * This is the method of the AsyncTaskLoader that will load and parse the JSON data
             * from OpenWeatherMap in the background.
             *
             * @return Weather data from OpenWeatherMap as an array of Strings.
             *         null if an error occurs
             */
            @Override
            public ArrayList<FoursquareModel> loadInBackground() {
                if (position != null) {
                    URL weatherRequestUrl = NetworkUtils.buildUrl(position.getLatitude(), position.getLongitude());
                    try {
                        String jsonPoiResponse = NetworkUtils.
                                getResponseFromHttpUrl(weatherRequestUrl);
                        FoursquareJsonUtils foursquareJsonUtils = new FoursquareJsonUtils();
                        return foursquareJsonUtils.getSimpleDataFrmJson(jsonPoiResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                } else {
                    return null;
                }
            }

            /**
             * Sends the result of the load to the registered listener.
             *
             * @param data The result of the load
             */
            public void deliverResult(ArrayList<FoursquareModel> data) {
                mVenuesData = data;
                super.deliverResult(data);
            }
        };
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<ArrayList<FoursquareModel>> loader, ArrayList<FoursquareModel> data) {
        if (null == data) {
            Log.v(TAG, "Loader finished without data.");
        } else {
            bestList = new ArrayList<>(data);

            nearestList = new ArrayList<>(data);

            BestPOI bPOI = (BestPOI) pagerAdapter.getItem(1);
            Collections.sort(bestList, new BestPoiComparator());

            bPOI.adapter.setItems(bestList);
            bPOI.adapter.notifyDataSetChanged();

            NearestPOI nPOI = (NearestPOI) pagerAdapter.getItem(0);
            Collections.sort(nearestList, new NearestPoiComparator());

            nPOI.adapter.setItems(nearestList);
            nPOI.adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<ArrayList<FoursquareModel>> loader) {

    }
    public class NearestPoiComparator implements Comparator<FoursquareModel> {
        @Override
        public int compare(FoursquareModel o1, FoursquareModel o2) {
            if (o1.getDistance() < o2.getDistance()) {
                return -1;
            } else if (o1.getDistance() > o2.getDistance()) {
                return 1;
            }
            return 0;
        }
    }
    class BestPoiComparator implements Comparator<FoursquareModel> {
        @Override
        public int compare(FoursquareModel o1, FoursquareModel o2) {
            if (o1.getCheckinsCount() > o2.getCheckinsCount()) {
                return -1;
            } else if (o1.getCheckinsCount() < o2.getCheckinsCount()) {
                return 1;
            }
            return 0;
        }
    }
}
