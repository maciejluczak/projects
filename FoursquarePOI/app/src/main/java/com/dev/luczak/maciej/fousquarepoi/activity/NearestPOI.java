package com.dev.luczak.maciej.fousquarepoi.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.luczak.maciej.fousquarepoi.model.FoursquareModel;
import com.dev.luczak.maciej.fousquarepoi.R;
import com.dev.luczak.maciej.fousquarepoi.adapter.RecyclerViewAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NearestPOI extends Fragment implements RecyclerViewAdapter.VenuesAdapterOnClickHandler {

    RecyclerView recyclerView;
    public RecyclerViewAdapter adapter;
    public ArrayList<FoursquareModel> nearestList;
    public NearestPOI() {
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v("Nearest","Resume");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        nearestList = ((MainActivity) getActivity()).nearestList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nearest_poi, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new RecyclerViewAdapter(this);
        adapter.setItems(nearestList);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(String venuesDetail) {
        Context context = getActivity();
        Class destinationClass = DetailActivity.class;
        Intent intentToStartDetailActivity = new Intent(context, destinationClass);
        intentToStartDetailActivity.putExtra(Intent.EXTRA_TEXT, venuesDetail);
        startActivity(intentToStartDetailActivity);
    }

}
