package com.dev.luczak.maciej.fousquarepoi.activity;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.dev.luczak.maciej.fousquarepoi.model.FoursquareModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.dev.luczak.maciej.fousquarepoi.R;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private static final String TAG="MapsActivity";
    private ArrayList<FoursquareModel> aListModel;
    private double latitude=0.0;
    private double longitude=0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        aListModel = new ArrayList<FoursquareModel>();
        aListModel = (ArrayList<FoursquareModel>) getIntent().getSerializableExtra(getString(R.string.model_extra));
        latitude = getIntent().getDoubleExtra(getString(R.string.latitude_extra), latitude);
        longitude = getIntent().getDoubleExtra(getString(R.string.longitude_extra), longitude);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.v(TAG,"onMapReady");
        for (FoursquareModel venuesPoint: aListModel) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(venuesPoint.getLatitude(),
                    venuesPoint.getLongitude())).title(venuesPoint.getName()));
        }
        Log.v(TAG,String.valueOf(latitude));
        Log.v(TAG,String.valueOf(longitude));
        LatLng localization= new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(localization).title("You!"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(localization,10.0f));
    }
}
