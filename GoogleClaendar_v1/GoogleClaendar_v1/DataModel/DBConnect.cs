﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace GoogleClaendar_v1.DataModel
{
    class DBConnect
    {
        private SqlConnection myConnection;
        public DBConnect()
        {
            myConnection = new SqlConnection("Password=123;Persist Security Info=True;User ID=sa;Initial Catalog=Vientos_v2;Data Source=PL_GARDENS");
            try
            {
                myConnection.Open();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        public void readData(Appointments app, DateTime currDate){
            try
            {
                DateTime curr1 = new DateTime(currDate.Year, currDate.Month, currDate.Day).AddDays(-1);
                DateTime curr2 = new DateTime(currDate.Year, currDate.Month, currDate.Day).AddDays(1);
                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand("select * from Calendar where Date > '"+curr1+"' and Date < '"+curr2+"'", myConnection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    app.Add(new Appointment() { Subject = myReader["Title"].ToString(), StartTime = (DateTime)myReader["Date"], EndTime = (DateTime)myReader["EndDate"] });
                    //Console.WriteLine(myReader["Title"].ToString());
                }
                myReader.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
