﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using GoogleClaendar_v1.CalendarTools;
namespace GoogleClaendar_v1
{
    class SelectionList<T> : List<SelectionItem<T>>, INotifyPropertyChanged
    {
        private int selectionCount;
        public event PropertyChangedEventHandler PropertyChanged;

        public int SelectionCount
        {
            get { return selectionCount; }
            set { 
                selectionCount = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("SelectionCount"));
            }
        }
        
        void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var item = sender as SelectionItem<T>;
            if ((item != null) && e.PropertyName == "IsSelected")
            {
                if (item.IsSelected)
                {
                    SelectionCount = SelectionCount + 1;
                }
                else
                    SelectionCount = SelectionCount - 1;
            }
        }
       /* SelectionItem<T> item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var item = sender as SelectionItem<T>;
            if ((item != null) && e.PropertyName == "IsSelected")
            {
                if (item.IsSelected)
                {
                    SelectionCount = SelectionCount + 1;
                    return item;
                }
                else
                {
                    SelectionCount = SelectionCount - 1;
                    return null;
                }
            }
            return null;
        }*/
        

        public SelectionList(IEnumerable<T> elements)
        {
            foreach (T element in elements)
                AddItem(element);
        }

        public void AddItem(T element)
        {
            var item = new SelectionItem<T>(element,false);
            item.PropertyChanged += item_PropertyChanged;

            Add(item);
        }
        public IEnumerable<T> GetSelection()
        {
            return this.Where(e => e.IsSelected).Select(e => e.Element);
        }

        public IEnumerable<U> GetSelection<U>(Func<SelectionItem<T>, U> expression)
        {
            return this.Where(e => e.IsSelected).Select(expression);
        }

        public IEnumerable<U> GetUnSelection<U>(Func<SelectionItem<T>, U> expression)
        {
            return this.Where(e => !e.IsSelected).Select(expression);
        }
    }
}
