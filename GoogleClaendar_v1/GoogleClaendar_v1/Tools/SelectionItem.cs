﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace GoogleClaendar_v1
{
    class SelectionItem<T> : INotifyPropertyChanged
    {
        private bool isSelected;
        public T Element { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public SelectionItem(T element, bool isSlected)
        {
            this.Element = element;
            this.IsSelected = isSlected;
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set { 
                isSelected = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsSelected"));
            }
        }

    }
}
