﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GoogleClaendar_v1.CalendarTools;
using GoogleClaendar_v1.Controls;
using GoogleClaendar_v1.DataModel;
namespace GoogleClaendar_v1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public GoogleAuth calInstance;
        private SelectionList<GoogleCalendar> listOfCalendars;
        private Appointments appointments = new Appointments();
        private DateTime currentDate;
        private DBConnect dbConnect;
        public MainWindow()
        {
            currentDate = DateTime.Now;

            InitializeComponent();
            calInstance = new GoogleAuth();
            //this.WebBrowserAuth.Navigate(calInstance.GetAutenticationURI());
            calInstance.connectCalendar();
            System.Threading.Thread.Sleep(5000);
            listOfCalendars= new SelectionList<GoogleCalendar>(calInstance.ChoseCalendar());
            myList.ItemsSource = listOfCalendars;
            listOfCalendars.PropertyChanged += list_PropertyChanged;
            DataContext = appointments;
            dbConnect = new DBConnect();
        }
        void pobieraj()
        {
            appointments.Clear();
            IEnumerable<GoogleCalendar> selection = listOfCalendars.GetSelection(elt => elt.Element);
            foreach (GoogleCalendar calend in selection)
            {
                calend.GetEventsByDate(currentDate);
                calend.Visible = true;
                foreach (CalendarEvent eventCal in calend.CalendarEvents)
                {
                    Console.WriteLine(eventCal.Title + '-' + eventCal.Id + '-' + eventCal.Description);
                    appointments.Add(new Appointment() { Subject = eventCal.Title, StartTime = (DateTime)eventCal.StartDate, EndTime = (DateTime)eventCal.EndDate });
                }
            }

            IEnumerable<GoogleCalendar> unSelection = listOfCalendars.GetUnSelection(elt => elt.Element);
            foreach (GoogleCalendar calend in selection)
            {
                calend.Visible = false;
            }
            dbConnect.readData(appointments,currentDate);
        }
        void list_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            pobieraj();
        }

        private void litleCalendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            var calendar = sender as Calendar;
            if (calendar.SelectedDate.HasValue)
            {
                currentDate = calendar.SelectedDate.Value;
                this.Title = currentDate.ToShortDateString();
                pobieraj();
            }
        }
    }
}
