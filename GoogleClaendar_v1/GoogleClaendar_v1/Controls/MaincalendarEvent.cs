﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GoogleClaendar_v1.Controls
{
    public class MainCalendarEvent : ContentControl
    {
        static MainCalendarEvent()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MainCalendarEvent), new FrameworkPropertyMetadata(typeof(MainCalendarEvent)));
        }

        public static readonly DependencyProperty StartTimeProperty =
            CalendarDayPanelTemplate.StartTimeProperty.AddOwner(typeof(MainCalendarEvent));

        public bool StartTime
        {
            get { return (bool)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        public static readonly DependencyProperty EndTimeProperty =
            CalendarDayPanelTemplate.EndTimeProperty.AddOwner(typeof(MainCalendarEvent));

        public bool EndTime
        {
            get { return (bool)GetValue(EndTimeProperty); }
            set { SetValue(EndTimeProperty, value); }
        }

    }
}
