﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using GoogleClaendar_v1.CalendarTools;
using GoogleClaendar_v1.DataModel;

namespace GoogleClaendar_v1.Controls
{
    public class MainCalendar : Control
    {
        static MainCalendar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MainCalendar), new FrameworkPropertyMetadata(typeof(MainCalendar)));
            CommandManager.RegisterClassCommandBinding(typeof(MainCalendar), new CommandBinding(Refreach, new ExecutedRoutedEventHandler(OnExecutedRefreach), new CanExecuteRoutedEventHandler(OnCanExecuteRefreach)));
        }

        public static readonly DependencyProperty CurrentDateProperty =
            DependencyProperty.Register("CurrentDate", typeof(DateTime), typeof(MainCalendar),
                new FrameworkPropertyMetadata((DateTime)DateTime.Now,
                    new PropertyChangedCallback(OnCurrentDateChanged)));
        public DateTime CurrentDate
        {
            get { return (DateTime)GetValue(CurrentDateProperty); }
            set { SetValue(CurrentDateProperty, value); }
        }
        private static void OnCurrentDateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((MainCalendar)d).OnCurrentDateChanged(e);
        }
        protected virtual void OnCurrentDateChanged(DependencyPropertyChangedEventArgs e)
        {
            CurrentDate = new DateTime(CurrentDate.Year, CurrentDate.Month, CurrentDate.Day);
            FilterAppointments();
        }


        public static readonly DependencyProperty AppointmentsProperty =
                DependencyProperty.Register("Appointments", typeof(IEnumerable<Appointment>), typeof(MainCalendar),
                    new FrameworkPropertyMetadata(null, new PropertyChangedCallback(MainCalendar.OnAppointmentsChanged)));

        public IEnumerable<Appointment> Appointments
        {
            get { return (IEnumerable<Appointment>)GetValue(AppointmentsProperty); }
            set { SetValue(AppointmentsProperty, value); }
        }

        private static void OnAppointmentsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((MainCalendar)d).OnAppointmentsChanged(e);
        }

        protected virtual void OnAppointmentsChanged(DependencyPropertyChangedEventArgs e)
        {
            FilterAppointments();
        }


        private void FilterAppointments()
        {
            DateTime byDate;
            if (CurrentDate == null) {
                byDate = (DateTime)DateTime.Now;
            }
            else { 
                byDate = CurrentDate;
            }
            MainCalendarDay day = this.GetTemplateChild("day") as MainCalendarDay;
            IEnumerable<Appointment> app;
            app = Appointments.ByDate(byDate);
            if (app == null)
                day.ItemsSource = app; 

           /* TextBlock dayHeader = this.GetTemplateChild("dayHeader") as TextBlock;
            dayHeader.Text = byDate.DayOfWeek.ToString();*/
        }

        protected virtual void OnCalendarListChanged(DependencyPropertyChangedEventArgs e)
        {
            
        }
        public static readonly RoutedCommand Refreach = new RoutedCommand("Refreach", typeof(MainCalendar));
        private static void OnCanExecuteRefreach(object sender, CanExecuteRoutedEventArgs e)
        {
            ((MainCalendar)sender).OnCanExecuteRefreach(e);
        }
        private static void OnExecutedRefreach(object sender, ExecutedRoutedEventArgs e)
        {
            ((MainCalendar)sender).OnExecutedRefreach(e);
        }
        protected virtual void OnCanExecuteRefreach(CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = false;
        }
        protected virtual void OnExecutedRefreach(ExecutedRoutedEventArgs e)
        {
            foreach (Appointment app in Appointments)
            {
                Console.WriteLine(app.Subject);
            }
            Console.WriteLine(CurrentDate);
            DateTime byDate = CurrentDate;
            MainCalendarDay day = this.GetTemplateChild("day") as MainCalendarDay;
            day.ItemsSource = Appointments.ByDate(byDate);

            e.Handled = true;
        }
    }
}
