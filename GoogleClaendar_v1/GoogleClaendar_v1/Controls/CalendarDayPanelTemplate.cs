﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GoogleClaendar_v1.Controls
{
    public class CalendarDayPanelTemplate : Panel
    {
        public static readonly DependencyProperty StartTimeProperty =
            DependencyProperty.RegisterAttached("StartTime", typeof(DateTime), typeof(CalendarDayPanelTemplate),
                new FrameworkPropertyMetadata((DateTime)DateTime.Now));

        public static DateTime GetStartTime(DependencyObject d)
        {
            return (DateTime)d.GetValue(StartTimeProperty);
        }

        public static void SetStartTime(DependencyObject d, DateTime value)
        {
            d.SetValue(StartTimeProperty, value);
        }


        public static readonly DependencyProperty EndTimeProperty =
            DependencyProperty.RegisterAttached("EndTime", typeof(DateTime), typeof(CalendarDayPanelTemplate),
                new FrameworkPropertyMetadata((DateTime)DateTime.Now));

        public static DateTime GetEndTime(DependencyObject d)
        {
            return (DateTime)d.GetValue(EndTimeProperty);
        }

        public static void SetEndTime(DependencyObject d, DateTime value)
        {
            d.SetValue(EndTimeProperty, value);
        }


        protected override Size MeasureOverride(Size availableSize)
        {
            Size size = new Size(double.PositiveInfinity, double.PositiveInfinity);

            foreach (UIElement element in this.Children)
            {
                element.Measure(size);
            }

            return new Size();
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            double top = 0;
            double left = 0;
            double width = 0;
            double height = 0;

            foreach (UIElement element in this.Children)
            {
                Nullable<DateTime> startTime = element.GetValue(CalendarDayPanelTemplate.StartTimeProperty) as Nullable<DateTime>;
                Nullable<DateTime> endTime = element.GetValue(CalendarDayPanelTemplate.EndTimeProperty) as Nullable<DateTime>;
                Console.WriteLine(startTime+" "+endTime);
                double start_minutes = (startTime.Value.Hour * 60) + startTime.Value.Minute;
                double end_minutes = (endTime.Value.Hour * 60) + endTime.Value.Minute;
                double start_offset = (finalSize.Height / (24 * 60)) * start_minutes;
                double end_offset = (finalSize.Height / (24 * 60)) * end_minutes;

                top = start_offset + 1;

                width = finalSize.Width;
                height = (end_offset - start_offset) - 2;

                element.Arrange(new Rect(left, top, width, height));
            }

            return finalSize;
        }
    }
}
