﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace GoogleClaendar_v1.Controls
{
    public class MainCalendarDayItem : ButtonBase
    {
        static MainCalendarDayItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MainCalendarDayItem), new FrameworkPropertyMetadata(typeof(MainCalendarDayItem)));
        }

        private void RaiseAddAppointmentEvent()
        {
            OnAddAppointment(new RoutedEventArgs(AddAppointmentEvent, this));
        }

        public static readonly RoutedEvent AddAppointmentEvent = 
            EventManager.RegisterRoutedEvent("AddAppointment", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(MainCalendarDayItem));

        public event RoutedEventHandler AddAppointment
        {
            add
            {
                AddHandler(AddAppointmentEvent, value);
            }
            remove
            {
                RemoveHandler(AddAppointmentEvent, value);
            }
        }

        protected virtual void OnAddAppointment(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        protected override void OnClick()
        {
            base.OnClick();

            RaiseAddAppointmentEvent();
        }
    }
}
