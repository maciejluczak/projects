﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GoogleClaendar_v1.Controls
{

    public class MainCalendarLegendItems : Control
    {
        static MainCalendarLegendItems()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MainCalendarLegendItems), new FrameworkPropertyMetadata(typeof(MainCalendarLegendItems)));
        }

        public static DependencyProperty HoursProperty =
            DependencyProperty.Register("Hours", typeof(string), typeof(MainCalendarLegendItems),
                new FrameworkPropertyMetadata((string)string.Empty));

        public string Hours
        {
            get { return (string)GetValue(HoursProperty); }
            set { SetValue(HoursProperty, value); }
        }

        public static  DependencyProperty MinutesProperty =
            DependencyProperty.Register("Minutes", typeof(string), typeof(MainCalendarLegendItems),
                new FrameworkPropertyMetadata((string)string.Empty));

        public string Minutes
        {
            get { return (string)GetValue(MinutesProperty); }
            set { SetValue(MinutesProperty, value); }
        }

    }
}
