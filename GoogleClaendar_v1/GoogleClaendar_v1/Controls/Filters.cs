﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoogleClaendar_v1.DataModel;

namespace GoogleClaendar_v1.Controls
{
    public static class Filters
    {
        public static IEnumerable<Appointment> ByDate(this IEnumerable<Appointment> appointments, DateTime date)
        {
            var app = from a in appointments
                      where a.StartTime.ToShortDateString() == date.ToShortDateString()
                      select a;
            return app;
        }
    }
}
