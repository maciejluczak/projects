﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace GoogleClaendar_v1.CalendarTools
{
    public class GoogleAuth
    {
        public static CalendarService calendarConnection;

       /* public Uri GetAutenticationURI()
        {
            List<string> postData = new List<string>();
            postData.Add("client_id=765335409132-h22lesgsknjkohs7p4dui6o5m99rn7lv.apps.googleusercontent.com");
            postData.Add("redirect_uri=urn:ietf:wg:oauth:2.0:oob");
            postData.Add("scope=https://www.googleapis.com/auth/calendar");
            postData.Add("response_type=code");
            return new Uri("https://accounts.google.com/o/oauth2/auth" + "?" + string.Join("&", postData.ToArray()));
        }*/
        public bool connectCalendar()
        {
            ClientSecrets secrets = new ClientSecrets
            {
                ClientId = "765335409132-h22lesgsknjkohs7p4dui6o5m99rn7lv.apps.googleusercontent.com",
                ClientSecret = "oKosT3aKUeC-hytjXH2K-_MN"
            };

            try
            {
                UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        secrets,
                        new string[]
                        { 
                                CalendarService.Scope.Calendar
                        },
                        "user2",
                        CancellationToken.None)
                .Result;

                var initializer = new BaseClientService.Initializer();
                initializer.HttpClientInitializer = credential;
                initializer.ApplicationName = "GAMCalendar";
                calendarConnection = new CalendarService(initializer);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        public List<GoogleCalendar> ChoseCalendar()
        {
            List<GoogleCalendar> lista = new List<GoogleCalendar>();
            var calendars = calendarConnection.CalendarList.List().Execute().Items;
            foreach (CalendarListEntry entry in calendars)
            {
                lista.Add(new GoogleCalendar(entry.Id, entry.Summary));
            }
            return lista;
        }



    }
}
