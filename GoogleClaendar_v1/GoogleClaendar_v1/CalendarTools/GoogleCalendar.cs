﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;

namespace GoogleClaendar_v1.CalendarTools
{
    public class GoogleCalendar
    {
        public GoogleCalendar(string id, string name)
        {
            this.Id = id;
            this.Name = name;
            this.Visible = false;
        }

        private string id;
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private bool visible;
        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }


        private List<CalendarEvent> calendarEvents;
        public List<CalendarEvent> CalendarEvents
        {
            get { return calendarEvents; }
            set { calendarEvents = value; }
        }

        public void GetEventsByDate(DateTime startDate)
        {
            calendarEvents = new List<CalendarEvent>();
            try
            {
                EventsResource.ListRequest req = GoogleAuth.calendarConnection.Events.List(this.Id);
                req.TimeMin = Convert.ToDateTime(startDate);
                req.TimeMax = Convert.ToDateTime(startDate).AddDays(1.0);
                req.SingleEvents = true;
                req.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;
                Events events = req.Execute();

                foreach (Event eventdata in events.Items)
                {
                    CalendarEvent item = new CalendarEvent
                    {
                        Id = eventdata.Id,
                        Title = eventdata.Summary,
                        Description = eventdata.Description,
                        Location = eventdata.Location,
                        IsBusy = (eventdata.Transparency != "transparent") ? true : false
                    };
                    EventDateTime start = eventdata.Start;
                    item.StartDate = start.DateTime;
                    item.StartTime = Convert.ToDateTime(start.DateTime).ToString("HH:mm");
                    EventDateTime end = eventdata.End;
                    item.EndDate = end.DateTime;
                    item.EndTime = Convert.ToDateTime(end.DateTime).ToString("HH:mm");

                    calendarEvents.Add(item);
                }
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}
